/*
    Create a function which is able to receive a single argument and add the input at the end of the users array.
    Function should be able to receive a single argument.
    Add the input data at the end of the array.
    The function should not be able to return data.
    Invoke and add an argument to be passed in the function.
    Log the users array in the console.
*/

console.log("Original Array:");

let users = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];
console.log(users);

function addUser(user) {
    users.push(user);
    console.log(users);
} 

addUser("John Cena");


/*
    Create a function which is able to receive an index number as a single argument return the item accessed by its index.
    function should be able to receive a single argument.
    return the item accessed by the index.
    Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
    log the itemFound variable in the console.

*/

let players = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista']
let itemFound;

function newArr(index) {
    return players[index];
}

itemFound = newArr(2);
console.log(itemFound);



/*
    Create a function which is able to delete the last item in the array and return the deleted item.
    Create a function scoped variable to store the last item in the users array.
    Shorten the length of the array by at least 1 to delete the last item.
    Return the last item in the array which was stored in the function scoped variable.
    Create a global scoped variable outside of the function and store the result of the function.
    Log the global scoped variable in the console.
*/

let athletes = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista', 'John Cena']
let deletedAthlete;

function deleteLastAthlete() {
    let lastAthlete = athletes[athletes.length - 1];
    athletes.length = athletes.length - 1;
    return lastAthlete;
}
deletedAthlete = deleteLastAthlete();
console.log(deletedAthlete);

/*
    Create a function which is able to update a specific item in the array by its index.
    Function should be able to receive 2 arguments, the update and the index number.
    First, access and locate the item by its index then re-assign the item with the update.
    This function should not have a return.
    Invoke the function and add the update and index number as arguments.
    Outside of the function, Log the users array in the console.
*/

let champs = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista']
console.log(champs);

function updateChamps(index, update) {
    champs[index] = update;
}

updateChamps(3, 'Triple H');
console.log(champs);



/*
    Create a function which is able to delete all items in the array.
    You can modify/set the length of the array.
    The function should not return anything.
    Invoke the function.
    Outside of the function, Log the users array in the console.
*/

let playersArr = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista']
let isArrayEmpty;

function clearPlayers() {
    playersArr.length = 0;
}

clearPlayers();
isArrayEmpty = (playersArr.length === 0);
console.log(playersArr);
console.log(isArrayEmpty);















